<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Laravel || Ventas</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="js/jquery-3.2.0.min.js"></script>
</body>
</html>